function soal_pertama(string, queries) {
    let alphabet = 'abcdefghijklmnopqrstuvwxyz';
    let alphabetSplit = alphabet.split('');
    
    let listOfAlphabet = [];
    for (let a =0; a < alphabetSplit.length; a++) {
        listOfAlphabet.push({
            key: alphabetSplit[a],
            value: a+1
        })
    }

    let previous = '';
    let newResult = [];
    for (let i = 0; i < string.length; i++) {
        let weight = 0;
        if (previous.includes(string[i])) {
            previous = previous + string[i];
            for (let x = 0; x < listOfAlphabet.length; x++){
                if (listOfAlphabet[x].key == string[i]) {
                    weight = listOfAlphabet[x].value * previous.length;
                    newResult.push(weight);
                }
            }
        } else {
            previous = string[i];
            for (let x = 0; x < listOfAlphabet.length; x++) {
                if (listOfAlphabet[x].key == string[i]){
                    weight = listOfAlphabet[x].value;
                    newResult.push(weight);
                }
            }
        }
    }

    let finalresult = [];
    for (let a = 0; a < queries.length; a++) {
        if (newResult.includes(queries[a])) {
            finalresult.push("Yes")
        } else{
            finalresult.push("No");
        }
    }
    return finalresult;
}

console.log(soal_pertama('abbcccd', [1, 3, 9, 8]));