function soal_ketiga(string) 
{ 
    let brackets = []; 
    for (let a = 0; a < string.length; a++) { 
        let bracket = string[a]; 
  
        if (bracket == '(' || bracket == '[' || bracket == '{') 
        { 
            brackets.push(bracket); 
            continue; 
        } 

        let checkBracket; 
        if (bracket === ')') {
            checkBracket = brackets.pop();
            if (checkBracket == '{' || checkBracket == '[') return "NO"; 
        } else if (bracket == '}') {
            checkBracket = brackets.pop();
            if (checkBracket == '(' || checkBracket == '[') return "NO"; 
        } else if (bracket == ']') {
            checkBracket = brackets.pop();
            if (checkBracket == '(' || checkBracket == '{') return "NO"; 
        }
    } 

    if (brackets.length == 0) {
        return "YES";
    }
} 

console.log(soal_ketiga('{ [ ( ) ] }'));
console.log(soal_ketiga('{ [ ( ] ) }'));
console.log(soal_ketiga('{ ( ( [ ] ) [ ] ) [ ] }'));